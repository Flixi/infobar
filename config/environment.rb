# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Infobar::Application.initialize!


WEEKDAYS = %w[Sonntag Montag Dienstag Mittwoch Donnerstag Freitag Samstag]