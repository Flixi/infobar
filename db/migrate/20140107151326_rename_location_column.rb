class RenameLocationColumn < ActiveRecord::Migration
  def change
    remove_column :happy_hours, :location_id
    add_column :happy_hours, :Location_id, :integer
  end
end
