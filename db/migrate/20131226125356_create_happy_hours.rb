class CreateHappyHours < ActiveRecord::Migration
  def change
    create_table :happy_hours do |t|
      t.integer :location_id
      t.integer :happyhourcategory_id
      t.time :start
      t.time :end
      t.integer :day

      t.timestamps
    end
  end
end
