class RenameLocationColumn < ActiveRecord::Migration
  def change
  	rename_column :happy_hours, :location_id, :Location_id
  end
end
