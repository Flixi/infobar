class CreateHappyHourItems < ActiveRecord::Migration
  def change
    create_table :happy_hour_items do |t|
      t.string :drink
      t.integer :happyhour_id
      t.float :price

      t.timestamps
    end
  end
end
