class RemoveFalseColumns < ActiveRecord::Migration
  def change
  	remove_column :locations, :LocationCategory_id
  	remove_column :happy_hours, :HappyHourCategory_id
  end
end
