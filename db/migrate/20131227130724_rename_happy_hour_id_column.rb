class RenameHappyHourIdColumn < ActiveRecord::Migration
  def change
  	rename_column :happy_hour_items, :happyhour_id, :happy_hour_id
  end
end
