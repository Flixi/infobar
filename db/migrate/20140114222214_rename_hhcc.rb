class RenameHhcc < ActiveRecord::Migration
  def change
  	rename_column :happy_hours, :HappyHourCategory_id, :happy_hour_category_id
  end
end
