class CreateHappyHourCategories < ActiveRecord::Migration
  def change
    create_table :happy_hour_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
