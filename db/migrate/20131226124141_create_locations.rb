class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.float :lat
      t.float :lng
      t.text :description
      t.integer :locationcategory_id

      t.timestamps
    end
  end
end
