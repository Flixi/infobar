class FixRenameBug < ActiveRecord::Migration
  def change
  	rename_column :happy_hours, :Location_id, :location_id
  	rename_column :locations, :LocationCategory_id, :location_category_id
  end
end
