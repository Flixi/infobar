class AddHappyHourCategoryId2 < ActiveRecord::Migration
  def change
  	remove_column :happy_hours, :happyhourcategory_id, :integer
  	add_column :happy_hours, :HappyHourCategory_id, :integer
  end
end
