class RemoveForeignKeys < ActiveRecord::Migration
  def change
  	remove_column :locations, :locationcategory_id
  	remove_column :happy_hours, :happyhourcategory_id
  end
end
