class Location < ActiveRecord::Base
	belongs_to :location_category
	has_many :happy_hours

	validates_presence_of :location_category, :name, :lat, :lng
end
