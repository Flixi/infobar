class HappyHourCategory < ActiveRecord::Base

	has_many :happy_hours
	
	validates_presence_of :name
end
