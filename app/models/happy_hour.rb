class HappyHour < ActiveRecord::Base

	belongs_to :happy_hour_category
	belongs_to :location
	has_many :happy_hour_items

	validates_presence_of :location, :happy_hour_category, :day, :start, :end, :happy_hour_items

	accepts_nested_attributes_for :happy_hour_items, :allow_destroy => true


	def self.current_happyhours(category_id)

		wday = get_functional_wday
		current_happyhours = Array.new
		happyhours = HappyHour.where("happy_hour_category_id= ? AND day = ?", category_id, wday)

		happyhours.each do |happyhour|

			start = get_functional_time(happyhour.start)
			ende = get_functional_time(happyhour.end)
			now = get_functional_time(Time.now)

			if (start < now && ende > now) 
				current_happyhours.push(happyhour)
			end
		end

		return current_happyhours

	end

	def self.upcoming_happyhours(category_id)

		wday = get_functional_wday
		upcoming_happyhours = Array.new
		happyhours = HappyHour.where("happy_hour_category_id = ? AND day = ?", category_id, wday)

		happyhours.each do |happyhour|

			start = get_functional_time(happyhour.start)
			now = get_functional_time(Time.now)

			if (start > now) 
				upcoming_happyhours.push(happyhour)
			end
		end

		return upcoming_happyhours
	end

	private

	def self.get_functional_time(time)

		hour = time.strftime("%H")
		minute = time.strftime("%M")
		hour = hour.to_i

		if hour < 5 
			hour = hour + 24
		end

		hour = hour.to_s

		functional_time = hour + ":" + minute
		return functional_time

	end

	def self.get_functional_wday 
		time = Time.new()
		wday = time.wday
		hour = Time.now.strftime("%H").to_i

		if(hour < 4) 

			if(wday == 0)
				wday = 6
				return wday
			end

			wday = wday - 1
			return wday
		end

		return wday
	end
end
