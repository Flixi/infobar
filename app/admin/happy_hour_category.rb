ActiveAdmin.register HappyHourCategory do

  permit_params do 
    permitted = [:name]
    permitted
  end

  index do 
    column "ID", :id
    column "Name", :name
    column "HappyHours" do |category| 
      category.happy_hours.count
    end
    default_actions
  end  
end
