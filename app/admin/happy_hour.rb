ActiveAdmin.register HappyHour do

  controller do
    def permitted_params
      params.permit!
    end
  end

  index do 
    column "ID", :id
    column "Kategorie", :happy_hour_category
    column "Wochentag" do |happyhour|
      WEEKDAYS[happyhour.day]
    end
    column "Location", :location
    column "Items" do |happyhour|
      happyhour.happy_hour_items.count
    end
    column "Start" do |happyhour|
      happyhour.start.strftime("%H:%M")
    end
    column "Ende" do |happyhour|
      happyhour.end.strftime("%H:%M")
    end
    column "Zuletzt geaendert" do |happyhour|
      happyhour.updated_at.strftime("%d. %B %Y")
    end
    default_actions
  end

  form do |f|
    f.inputs "Allgemeines" do 
      f.input :happy_hour_category, :label => "Kategorie", :as => :radio
      f.input :day, :as => :select, :collection => [["Montag", "1"], ["Dienstag", "2"], ["Mittwoch", "3"], ["Donerstag", "4"], ["Freitag", "5"], ["Samstag", "6"], ["Sonntag", "0"]], :label => "Wochentag"
      f.input :location
      f.input :start, :as => :time_picker, :label => "Anfang"
      f.input :end, :as => :time_picker, :label => "Ende"
    end
    f.inputs do
      f.has_many :happy_hour_items, :allow_destroy => true, :heading => 'HappyHours', :new_record => true do |cf|
        cf.input :drink, :label => "Getraenk/e"
        cf.input :price, :label => "Preis/Rabatt"
      end
    end
    f.actions
  end
end
