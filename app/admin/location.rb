ActiveAdmin.register Location do

  controller do
    def permitted_params
      params.permit!
    end
  end

  index do 
    column "ID", :id
    column "Kategorie", :location_category
    column "HappyHours" do |location|
      location.happy_hours.count
    end
    column "Zuletzt geaendert" do |location|
      location.updated_at.strftime("%d. %B %Y")
    end
    default_actions
  end 
end
