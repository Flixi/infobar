ActiveAdmin.register LocationCategory do


  permit_params do 
    permitted = [:name]
    permitted
  end

  index do 
    column "ID", :id
    column "Name", :name
    column "Locations" do |category|
      category.locations.count
    end
    default_actions
  end
end
